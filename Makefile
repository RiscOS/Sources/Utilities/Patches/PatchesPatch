# Copyright 1996 Acorn Computers Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# Makefile for Patch
#

COMPONENT  = Patch
override TARGET = !RunImage
OBJS       = Main MsgCode PatchApply PatchParse Subroutins
LIBS       = ${RLIB}
INSTTYPE   = app
INSTAPP_FILES = !Boot !Help !Run !RunImage Templates Messages \
                ReadMe BootStrap Transforms squeeze:Library \
                !Sprites:Themes !Sprites22:Themes !Sprites11:Themes \
                Morris4.!Sprites:Themes.Morris4 Morris4.!Sprites22:Themes.Morris4 \
                Ursula.!Sprites:Themes.Ursula Ursula.!Sprites22:Themes.Ursula
INSTAPP_VERSION = Messages
INSTAPP_DEPENDS = Patches UnSqueeze

include CApp

Patches:
	${MKDIR} ${INSTAPP}${SEP}Patches
	${CP} Resources${SEP}Patches${SEP}Advance  ${INSTAPP}${SEP}Patches${SEP}Advance  ${CPFLAGS}
	${CP} Resources${SEP}Patches${SEP}PocketFS ${INSTAPP}${SEP}Patches${SEP}PocketFS ${CPFLAGS}
	${CP} Resources${SEP}Patches${SEP}ProCAD+  ${INSTAPP}${SEP}Patches${SEP}ProCAD+  ${CPFLAGS}

UnSqueeze:
	@${RM} ${DIRS} # So the submakefile will create any dirs it needs
	@${MAKE} install TARGET=UnSqueeze INSTDIR=${INSTAPP}${SEP}Modules -f unsqueeze${EXT}mk

clean::
	@${MAKE} -f unsqueeze${EXT}mk clean
	@${STRIPDEPEND} unsqueeze${EXT}mk

# Dynamic dependencies:
